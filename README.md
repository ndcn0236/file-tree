[![PyPI - Downloads](https://img.shields.io/pypi/dm/file-tree)](https://pypi.org/project/file-tree/)
[![Documentation](https://img.shields.io/badge/Documentation-file--tree-blue)](https://open.win.ox.ac.uk/pages/fsl/file-tree/)
[![Documentation](https://img.shields.io/badge/Documentation-fsleyes-blue)](https://open.win.ox.ac.uk/pages/fsl/fsleyes/fsleyes/userdoc/filetree.html)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6576809.svg)](https://doi.org/10.5281/zenodo.6576809)
[![Pipeline status](https://git.fmrib.ox.ac.uk/fsl/file-tree/badges/master/pipeline.svg)](https://git.fmrib.ox.ac.uk/fsl/file-tree/-/pipelines)
[![Coverage report](https://git.fmrib.ox.ac.uk/fsl/file-tree/badges/master/coverage.svg)](https://open.win.ox.ac.uk/pages/fsl/file-tree/htmlcov)

File-tree has been moved to https://git.fmrib.ox.ac.uk/fsl/file-tree